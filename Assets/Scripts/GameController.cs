﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public Vector3 spawnPosition;
	public GameObject hazard;

	public GUIText scoreText;
	private int score;

	void Start() {
		score = 0;
		UpdateScore ();
		StartCoroutine (SpawnWaves());
	}

	IEnumerator SpawnWaves() {
		while (true) {
			yield return new WaitForSeconds (4.0f);
			for (int i = 0; i < 10; i++) {
				Vector3 position = new Vector3 (Random.Range (-spawnPosition.x, spawnPosition.x), spawnPosition.y, spawnPosition.z);
				GameObject newHazard = Instantiate (hazard, position, Quaternion.identity);
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	public void addScore(int newScoreValue) {
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore() {
		scoreText.text = "Score: " + score;
	}
}
