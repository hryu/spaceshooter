﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {

	public float tumble = 1.0f;

	void Start() {
		Rigidbody body = GetComponent<Rigidbody>();
		body.angularVelocity = Random.insideUnitSphere * (tumble + Random.value);
	}
}
