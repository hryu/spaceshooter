﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary {
	public float xmin, xmax, zmin, zmax;
}

public class PlayerController : MonoBehaviour {
    public float speed;
	public float tilt;
	public Boundary boundary;

	public GameObject shot;
	public float fireRate;
	private float nextFire;

	void Update() {
		if (Input.GetButton ("Fire1") && Time.time > nextFire) {
			nextFire = Time.time + fireRate;	
			Transform transf = GetComponent<Transform> ();
			Instantiate (shot, transf.position, transf.rotation);
			AudioSource audio = GetComponent<AudioSource> ();
			audio.Play ();
		}
	}

	void FixedUpdate()
    {
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
//        Debug.Log(moveHorizontal + " " + moveVertical);
        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		Rigidbody body = GetComponent<Rigidbody>();
		body.velocity = movement * speed;

		body.position = new Vector3
			(
				Mathf.Clamp(body.position.x, boundary.xmin, boundary.xmax),
				0.0f,
				Mathf.Clamp(body.position.z, boundary.zmin, boundary.zmax)
			);

		body.rotation = Quaternion.Euler (0.0f, 0.0f, moveHorizontal * -tilt);
//		Debug.Log(body.position.x + " " + body.position.z);
    }
}
